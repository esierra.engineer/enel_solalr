# SolArl

Archivos y códigos de procesamiento de datos de la planta SL.

## Requisitos
### python 
- pandas
### R
- data.table
- lubridate
- tidyverse
- padr
- arrow

## Descripcion
### Datos de entrada
Archivo Excel "25_05_2023_Solicitud CT09 (MMS+SI+TR).xlsx" en la carpeta "xls"
### 1-xls to csv
Como el Archivo Excel es complicado para procesar, debido a la lentitud del programa y del formato. Se trabajará con archivos *.csv.
Para esto se lee el archivo Excel y se guarda cada hoja por separado en un archivo CSV con el programa *excel_to_csv.py*.
### 2-csv to dataframes
Para mayor comodidad, se estandariza el formato de los datos, se crea un archivo para cada fuente (SI, TR, PPC, MMS) en formato *long* es decir *hacia abajo*.

Se requiere una lista en CSV de los nombres de las variables que contiene cada CSV del paso anterior **_csv_file_FUENTE_headers.csv_** . 

Se leen los CSV obtenidos del paso anterior con los programas *read_FUENTE_data.py* y se guardan como **_output\_FUENTE.csv_**
### R
Con los códigos de la carpeta *R* se hace la lectura de los CSV del paso anterior, y se guarda información agregada de los datos originales.

Los programas son llamados *read_FUENTE_data.R*

Los archivos de salida, en formato CSV y *feather* (de la librería *arrow*) quedan en la carpeta **_R/output_**

### 3-informe final
Con el programa *datos_informe_final.R* se producen los archivos CSV, dentro de la misma carpeta, con los datos del Informe Final.
