"""
Codigo para transformar la planilla excel en csv (uno por hoja)
excel_file: el archivo excel con la informacion
output_directory: el directorio de salida
sheet_name: el nombre de la hoja que se quiere leer
"""
import pandas as pd
from os.path import join


def excel_to_csv(excel_file, sheet_name, csv_file):
    # Read the Excel file
    df = pd.read_excel(excel_file, sheet_name=sheet_name)

    # Save the selected sheet as a CSV file
    df.to_csv(csv_file, index=False)


# Specify the file paths and names
excel_file = r"C:\git\solarl_enel\xls\25_05_2023_Solicitud CT09 (MMS+SI+TR).xlsx"
sheet_name = "MMs"  # Change this to the desired sheet name
output_directory = r"C:\git\solarl_enel\1-xls to csv"
csv_file = f'csv_file_{sheet_name}.csv'

# Call the function to convert Excel to CSV
excel_to_csv(excel_file, sheet_name, join(output_directory, csv_file))

"""
sheet names:
"TR 09"
"PPC-AI"
"SI09"
"MMs"
"""
