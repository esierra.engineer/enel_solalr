from csv import reader
import pandas as pd


csv_file = r'C:\git\solarl_enel\1-xls to csv\csv_file_TR 09.csv'
headers_file = r'C:\git\solarl_enel\2-csv to dataframes\csv_file_TR 09_headers.csv'
output_file = r'C:\git\solarl_enel\2-csv to dataframes\output_TR.csv'

data = pd.read_csv(csv_file, skiprows=1, header=None)
print(data)

output_list = list()
# open file in read mode
with open(headers_file, 'r') as read_obj:
    # pass the file object to reader() to get the reader object
    csv_reader = reader(read_obj)
    # Iterate over each row in the csv using reader object
    for row in csv_reader:
        try:
            # row variable is a list that represents a row in csv
            split_line = row[0].split(sep=r".")
            #print(split_line)
            this_row_dict = {
                "var_name": row[0].replace(r".", "_"),
                "element_1": split_line[2],
                "element_2": split_line[3],
                "mag_name": split_line[-3]
            }
        except IndexError:
            print(row)
            continue
        output_list.append(this_row_dict)

#%%

#print(output_list)
headers = pd.DataFrame.from_dict(output_list)
print(headers)

#%%

this_header_idx = 0
output_df = pd.DataFrame(columns=["datetime", "value", "var_name",
                                  "element_1", "element_2", "mag_name"])
for this_col_idx in range(0, len(data.columns), 2):
    this_vector_data = data.iloc[:, [this_col_idx, this_col_idx+1]]
    this_header = headers.iloc[this_header_idx, :]
    this_header_idx += 1
    this_vector_data_nonans = this_vector_data.dropna().copy()
    this_vector_data_nonans.columns = ["datetime", "value"]

    this_vector_data_nonans["var_name"] = this_header["var_name"]
    this_vector_data_nonans["element_1"] = this_header["element_1"]
    this_vector_data_nonans["element_2"] = this_header["element_2"]
    this_vector_data_nonans["mag_name"] = this_header["mag_name"]

    output_df = pd.concat([output_df, this_vector_data_nonans], ignore_index=True)


#%%

output_df.to_csv(output_file, index=False)
